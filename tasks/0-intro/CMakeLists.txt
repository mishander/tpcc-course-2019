cmake_minimum_required(VERSION 3.9)

add_subdirectory(deadlock)
add_subdirectory(dining)
add_subdirectory(guarded)
add_subdirectory(jump)
add_subdirectory(sleepsort)

