#include "channel.hpp"

#include <twist/test_framework/test_framework.hpp>
#include <twist/threading/test.hpp>

#include <twist/test_utils/barrier.hpp>
#include <twist/test_utils/count_down_latch.hpp>
#include <twist/test_utils/executor.hpp>
#include <twist/test_utils/mutex_tester.hpp>

#include <twist/threading/stdlike.hpp>

#include <twist/fault/inject_fault.hpp>

#include <string>
#include <vector>

////////////////////////////////////////////////////////////////////////////////

namespace channel {

class Tester {
 public:
  Tester(const TTestParameters& parameters)
    : parameters_(parameters),
      channel_(parameters_.Get(3)),
      start_barrier_(parameters.Get(0) + parameters.Get(1)),
      producer_count_(parameters_.Get(0)) {
  }

  // One-shot
  void Run() {
    twist::ScopedExecutor executor;
    size_t producers = parameters_.Get(0);
    for (size_t t = 0; t < producers; ++t) {
      executor.Submit(&Tester::RunProducerThread, this, t);
    }

    size_t consumers = parameters_.Get(1);
    for (size_t t = 0; t < consumers; ++t) {
      executor.Submit(&Tester::RunConsumerThread, this);
    }
    executor.Join();

    ASSERT_EQ(total_produced_.load(), total_consumed_.load());
  }

 private:
  void RunProducerThread(size_t thread_index) {
    start_barrier_.PassThrough();

    size_t items = parameters_.Get(2);
    size_t producers = parameters_.Get(0);
    for (size_t i = thread_index; i < items; i += producers) {
      channel_.Send(std::to_string(i));
      total_produced_.fetch_add(i);
    }

    if (producer_count_.fetch_sub(1) == 1) {  // Last producer
      channel_.Close();
    }
  }

  void RunConsumerThread() {
    start_barrier_.PassThrough();

    std::string message;
    while (channel_.Recv(message)) {
      int value = std::stoi(message);
      total_consumed_.fetch_add(value);
    }
  }

 private:
  TTestParameters parameters_;
  solutions::BufferedChannel<std::string> channel_;
  twist::OnePassBarrier start_barrier_;
  std::atomic<size_t> producer_count_{0};
  std::atomic<size_t> total_produced_{0};
  std::atomic<size_t> total_consumed_{0};
};

}

void ChannelStressTest(const TTestParameters& parameters) {
  channel::Tester(parameters).Run();
}

// Parameters: producers, consumers, items, channel capacity
T_TEST_CASES(ChannelStressTest)
  .TimeLimit(std::chrono::seconds(30))
  .Case({1, 1, 100000, 1})
  .Case({5, 5, 100000, 100})
  .Case({7, 2, 100000, 3})
  .Case({3, 9, 100000, 4});



////////////////////////////////////////////////////////////////////////////////

void LostWakeupStressTest(const TTestParameters& parameters) {
  size_t repeats = parameters.Get(0);
  for (size_t i = 0; i < repeats; ++i) {
    solutions::BufferedChannel<int> semaphore;

    twist::CountDownLatch consumers_latch{2};
    twist::OnePassBarrier producers_barrier{2};

    auto consumer = [&semaphore, &consumers_latch]() {
      consumers_latch.CountDown();
      int token;
      semaphore.Recv(token);
    };

    auto producer = [&semaphore, &producers_barrier]() {
      producers_barrier.PassThrough();
      semaphore.Send(1);  // Put token
    };

    twist::ScopedExecutor executor;

    executor.Submit(consumer);
    executor.Submit(consumer);

    consumers_latch.Await(); // Better than nothing

    executor.Submit(producer);
    executor.Submit(producer);

    executor.Join();
  }
}

// Parameters: iterations
T_TEST_CASES(LostWakeupStressTest)
  .TimeLimit(std::chrono::seconds(30))
  .Case({1000});

////////////////////////////////////////////////////////////////////////////////

void MissingCloseStressTest(const TTestParameters& parameters) {
  size_t repeats = parameters.Get(0);

  for (size_t i = 0; i < repeats; ++i) {
    twist::OnePassBarrier barrier{2};
    solutions::BufferedChannel<std::string> channel;

    auto recv_routine = [&]() {
      barrier.PassThrough();
      std::string item;
      ASSERT_FALSE(channel.Recv(item));
    };

    twist::th::thread consumer(recv_routine);

    barrier.PassThrough();
    channel.Close();

    consumer.join();
  }
}

// Parameters: iterations
T_TEST_CASES(MissingCloseStressTest)
  .TimeLimit(std::chrono::seconds(15))
  .Case({5000});

////////////////////////////////////////////////////////////////////////////////

RUN_ALL_TESTS()
