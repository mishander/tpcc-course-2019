import json

def find_benchmark(report, name):
    for benchmark in report["benchmarks"]:
        if benchmark["name"].startswith(name):
            return benchmark

    raise RuntimeError("Benchmark not found: {}".format(name))

def check_scores(report, private_report):
    for ref_benchmark in private_report["benchmarks"]:
        your_benchmark = find_benchmark(report, ref_benchmark["name"])

        your_time = your_benchmark["real_time"]
        ref_time = ref_benchmark["real_time"]

        if ref_time * 2.5 < your_time:
            return False, "Your solution is too slow on benchmark {}".format(ref_benchmark["name"])

        if ref_time * 1.5 < your_time:
            return True, "Your solution may be better!"

    return True, "OK"

